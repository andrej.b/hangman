from random import choice


class Hangman:
    def __init__(self):
        self.word = None
        self.lives = 5
        self.mask = None
        self.picked = []

    def get_word(self):
        file = open("HR_Txt-624.txt", "r")
        all_lines = file.readlines()
        while not self.word:
            split = choice(all_lines).split()
            if len(split) != 3:
                continue
            if split[2] == "imenica":
                self.word = split[1]

    # za rijec napravit string masku npr PAS => ___
    def set_mask(self):
        for _ in self.word:
            self.mask = f"{self.mask}_" if self.mask else "_"

    # resetiraj igru u prvobitno stanje (rijec, maska, lista odabira, zivoti) i odaberi novi rijec + maska
    def reset(self):
        self.word = None
        self.lives = 5
        self.mask = None
        self.picked = []
        self.get_word()
        self.set_mask()

    # validiraj da je uneseno jedno slovo, provjeri da li je vec odabrano to slovo ako nije dodaj u odabranu listu, provjeri slovo unutar rijeci (po potrebi oduzmi zivote), sve pozicije slova unutar maske zamijeniti sa slovom npr rijec SALATA, slovo A, maska _A_A_A
    def check_hit(self, letter):
        if len(letter) > 1:
            print("pick only one letter")
            return
        if letter in self.picked:
            print(f"letter {letter} already in use")
            return
        self.picked.append(letter)
        if letter not in self.word:
            self.lives -= 1
            return
        hits = [pos for pos, char in enumerate(self.word) if char == letter]
        for hit in hits:
            self.mask = self.mask[:hit] + letter + self.mask[hit + 1 :]

    def print_status(self):
        print(f"{self.mask} - {self.lives} live(s) remaining")

    # provjeri da je rijec odabrana ukoliko nije pozovi reset
    # dok maska != rijec
    # ukoliko su zivoti 0 ispisi nema zivota, ispisi trazenu rijec, resetiraj, continue while
    # unutar while a van uvjeta za zivot ispisi status igre, trazi slovo i provjeri check_hit
    # unutar whilea provjeri nakon hita mask == word, ako je ispisi poruku win, ispisi rijec, resetiraj
    def start(self):
        if not self.word:
            self.reset()
        while self.mask != self.word:
            if self.lives == 0:
                print("you're dead")
                print(f"word was: {self.word}")
                a = input("continue? y/n: ")
                if a.lower() == "y":
                    self.reset()
                    continue
                else:
                    break
            self.print_status()
            letter = input("input character: ")
            self.check_hit(letter)
            if self.mask == self.word:
                print("win")
                print(f"word is: {self.word}")
                a = input("continue? y/n: ")
                if a.lower() == "y":
                    self.reset()


game = Hangman()
try:
    game.start()
except KeyboardInterrupt:
    pass
